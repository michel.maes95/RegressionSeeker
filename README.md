# RegressionSeeker

Clone a repo
```
  git clone https://github.com/Maes95/SpringBootSamples.git
```

Write a config file:
```js
{
    "id": "BASIC",
    "explain": "Can't divide by 0",
    "project": "SpringBootSamples",
    "base_commit": "add8221fb5314265ce7d7a8a4002078a498511a3",
    "build": "./gradlew -Dskip.tests build > /dev/null 2>&1",
    "folder":  "src/test/java/samples/websocket/tomcat/divider/",
    "file": "DividerTest.java",
    "test_command": "mvn -Dtest=DividerTest#divideBy0 test",
    "test_report": "target/surefire-reports/TEST-samples.websocket.tomcat.divider.DividerTest.xml",
    "iterations": 5
}
```

Run pythpn script
```
  python searchRegression configFiles/BASIC-0.json
```

See output

```
michel@michel:~/Investigacion/RegressionSeeker$ python searchRegression.py configFiles/BASIC-0.json
add8221fb5 commit pass the test (FIXED COMMIT)
e20b61b3d2 commit doesn't pass the test
 ┗ ERROR: / by zero
eb79de4433 commit doesn't pass the test
 ┗ ERROR: / by zero
b4f9c22857 commit pass the test, eb79de4433 commit contains an error (REGRESSION)

    ########################################################
    #   The author of eb79de4 was Maes95, 6 days ago
    #   The title was >>Remove useless else branch<<
    ########################################################
```
