import subprocess
import os
import re
import json
import sys
import xml.etree.ElementTree as ET

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    GREY = '\u001b[38;5;245m'

message_format = """
    ########################################################
    #   The author of """+bcolors.HEADER+"""%h"""+bcolors.ENDC+""" was """+bcolors.BOLD+"""%an"""+bcolors.ENDC+""", %ar
    #   The title was >>"""+bcolors.WARNING+"""%s"""+bcolors.ENDC+"""<<
    ########################################################
"""

if len(sys.argv) != 2:
    print("Use: python script.py <config_file>")
    exit()
config = json.load(open(sys.argv[1]))

PREVIOUS_COMMITS=config["iterations"]
project=config["project"]
base_commit=config["base_commit"]
build= config["build"]
folder= config["folder"]
file_=config["file"]
source=folder+file_
test_command= config["test_command"]
test_report= config["test_report"]

outfile = open('logs/%s.log' % config['id'], 'w+')

def call(params, output=outfile):
    return subprocess.call(params, shell=True, stdout=output, stderr=outfile)

def get_previous_commit(n=1):
    with open('out', 'w') as out:
        call("git show HEAD~%d | head -n 1" % n, output=out)
    with open('out', 'r') as out:
        text = out.read()
        m = re.search(r'commit (\S*)', text)
        if m == None:
            print(bcolors.WARNING+"No more commits to check, the beginning of the project has been reached"+bcolors.ENDC)
            cleanExit()
        return m.group(1)
    call("rm out")

def change_commit(commit_hash):
    call("git checkout -f .")
    call("git checkout -f %s" % commit_hash)

def printAndSaveReport(commit):
    with open('../%s_report' % config['id'], 'w+') as out:
        call("git show %s" % commit, output=out)

    with open('aux', 'w+') as out:
        call("git show --format='%s' -s %s" % (message_format, commit), output=out)
    with open('aux', 'r') as out:
        print(out.read())
    call("rm aux")

def cleanExit():
    # RESTORE GIT HEAD
    call("rm %s" % source)
    call("rm ../testFile")
    change_commit(base_commit)
    outfile.close()
    exit()

def getFailures(report_path):
    tree = ET.parse(report_path)
    testsuite= tree.getroot()
    hasFails = False
    fails = []
    for case in testsuite.iter('testcase'):
        if case.find('failure') != None or case.find('error') != None:
            if case.find('failure') != None:
                fails.append("FAILURE: "+case.find('failure').attrib['message'])
            if case.find('error') != None:
                fails.append("ERROR: "+case.find('error').attrib['message'])
            hasFails=True
    return (hasFails, fails)

# MOVE TO PROJECT DIRECTORY
os.chdir(os.getcwd()+"/"+project)
change_commit(base_commit)

# GET TEST FILE OUT PROJECT
if not os.path.isfile(source):
    print("TEST FILE DOESN'T EXIST")
    exit()
call("cp %s %s"%(source, "../testFile"))

# CHECK TEST PASS
call(build)
returncode = call(test_command)
if returncode != 0:
    print("ERROR: NOT A SUCCESS TEST")
    exit()
print("\033[95m%s\033[0m commit \033[92mpass\033[0m the test (FIXED COMMIT)"%base_commit[0:10])

# CHECK IF TEST PASS FOR PREVIOUS COMMITS

last = base_commit
success_commits = [base_commit]
for _ in range(PREVIOUS_COMMITS):
    prev = get_previous_commit()
    call("rm -rf %s" % folder)
    change_commit(prev)
    call("mkdir -p %s" % folder)
    call("cp %s %s"%("../testFile", source))
    call(build)
    # REMOVE TEST REPORT
    call("rm %s" % test_report)
    # RUN TEST COMMAND TO GENERATE A NEW REPORT
    call(test_command)

    if not os.path.isfile(test_report):
        print("\033[95m%s\033[0m commit \033[91mhad an error\033[0m when test was running, no test report found"%prev[0:10])
    else:
        hasFails, fails = getFailures(test_report)
        if hasFails:
            print("\033[95m%s\033[0m commit \033[91mdoesn't pass\033[0m the test"%prev[0:10])
            for fail in fails[:-1]:
                print(bcolors.GREY+" ┣ "+fail+bcolors.ENDC)
            print(bcolors.GREY+" ┗ "+fails[-1]+bcolors.ENDC)
                #break # FOR NOW, ONLY FIRST FAIL (IF EXIST)
            last = prev
        else:
            if(success_commits[-1] == last):
                print("\033[95m%s\033[0m commit \033[92mpass\033[0m the test, no regression" % prev[0:10] )
                success_commits.append(prev)
                last = prev
            else:
                print("\033[95m%s\033[0m commit \033[92mpass\033[0m the test, \033[95m%s\033[0m commit contains an \033[91merror (REGRESSION)\033[0m"%(prev[0:10], last[0:10]))
                printAndSaveReport(last)
                cleanExit()


print("\nNO REGRESSION DETECTED IN %d PREVIOUS COMMITS" % PREVIOUS_COMMITS)
cleanExit()
